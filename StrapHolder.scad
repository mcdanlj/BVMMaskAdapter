// Copyright Michael K Johnson
// License: Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// https://creativecommons.org/licenses/by-nc/4.0/

// LEGAL DISCLAIMER: The 3D printed mask information presented here is 
// intended to assist the general public during the current global
// pandemic related to COVID-19 and the related nationwide shortage
// of personal protective equipment.  Please be aware that this mask
// design is not intended to replace standard protective equipment such
// as N-95 masks or surgical masks when that equipment is available.
// The use of these 3D printed masks has not been fully tested and
// Michael K Johnson and any other contributors assume no liability and
// make no representations, warranties, or guarantees regarding the
// safety, efficacy, or appropriate use of these masks in any particular
// situation. Each facility should test each batch of masks prior to
// relying on them for protection. Use of this information for any
// purpose is at the maker’s and user’s own risk.

include <Core.scad>

inner_d = fitting_large_d + clearance*4;
r=15;
straps=[ // x offset, strap_width, strap_thickness
    [-inner_d*2, 20, 4], // typical 20-22mm CPAP headgear straps
    [0, 12, 3],          // ½" elastic straps
    [inner_d*1.5, 7, 3]  // ¼" elastic straps
];

$fn=30;

module slot(w, t, h, strap_width, strap_thickness, r=0) {
    rotate([0, 0, r]) hull() for (i=[-1, 1]) {
        translate([0, i*(strap_width/2-strap_thickness/2), 0]) cylinder(d=t, h=h);
    }
}
module strap_adapter(strap_width, strap_thickness) {
    x_offset=inner_d/2+shell*4;
    y_offset=strap_width/2+shell/2;
    difference() {
        hull() {
            for (x=[-1, 1], y=[-1, 1]) {
                translate([x*x_offset, y*y_offset, 0]) slot(strap_width+shell*2, strap_thickness+shell*2, shell, strap_width, strap_thickness, r=x*y*r);
            }
        cylinder(d=inner_d+shell*4, h=shell, $fn=60);
        }
        for (x=[-1, 1], y=[-1, 1]) {
            translate([x*x_offset, y*y_offset, -1]) slot(strap_width, strap_thickness, shell+2, strap_width, strap_thickness, r=x*y*r);
        }
        translate([0, 0, -1]) cylinder(d=inner_d, h=shell+2, $fn=60);
    }
}
for (strap=straps) {
    translate([0, strap[0], 0]) strap_adapter(strap[1], strap[2]);
}