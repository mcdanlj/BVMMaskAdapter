// Copyright Michael K Johnson
// License: Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// https://creativecommons.org/licenses/by-nc/4.0/

// LEGAL DISCLAIMER: The 3D printed mask information presented here is 
// intended to assist the general public during the current global
// pandemic related to COVID-19 and the related nationwide shortage
// of personal protective equipment.  Please be aware that this mask
// design is not intended to replace standard protective equipment such
// as N-95 masks or surgical masks when that equipment is available.
// The use of these 3D printed masks has not been fully tested and
// Michael K Johnson and any other contributors assume no liability and
// make no representations, warranties, or guarantees regarding the
// safety, efficacy, or appropriate use of these masks in any particular
// situation. Each facility should test each batch of masks prior to
// relying on them for protection. Use of this information for any
// purpose is at the maker’s and user’s own risk.

include <Core.scad>

equivalent_diameter=80; // Make a triangle of approximately equal surface area to a circle of this diameter
base_length=sqrt(pow(equivalent_diameter/2, 2) * 3.14 * 2);

coupling_y=((base_length/2 - (fitting_top_d-post_d-shell))/sqrt(3))*2;
frame_extra = shell*2+interference+clearance;
frame_extra_outer = frame_extra + 2*shell * sqrt(2);

b_basis = base_length+frame_extra;
b_outer = b_basis + 2*shell;
b_inner = base_length-frame_extra;

// Triangular hull from circles, centered
// `radius` is radius of circles forming vertices
// `base` is max length parallel to a side
// `height` is Z height
module rounded_triangle(radius, base, height) {
    h=((base/2 - radius)/sqrt(3))*2; // distance fron center of triangle to center of corner circle
    hull() for (angle=[90:120:330]) {
        translate([cos(angle)*h, sin(angle)*h, 0]) cylinder(r=radius, h=height);
    }
}
module triangle_nubs(back=false, clearance=0) {
    // friction-fit nubs to hold frame to adapter
    major_r = (((base_length+frame_extra)/2-fitting_top_d)/sqrt(3))*2 + fitting_top_d;
    minor_r = (((b_basis-2*fitting_top_d)/2)/sqrt(3)) + fitting_top_d;
    for (angle=[90:120:330]) {
        // normal nubs at apex of radius
        translate([cos(angle)*major_r, sin(angle)*major_r, frame_height/2 + shell/2]) sphere(r=interference*0.8+clearance, $fn=12);
        // larger nubs along flat sides which flex more
        translate([cos(angle-60)*minor_r, sin(angle-60)*minor_r, frame_height/2 + shell/2]) sphere(r=interference*0.9+clearance, $fn=12);
    }
}
module triangle_body() {
    difference() {
        union() {
            // outer ring
            difference() {
                rounded_triangle(fitting_top_d, base_length+shell*2, height);
                translate([0, 0, -1]) rounded_triangle(fitting_top_d-shell, base_length, height+2);
                // fittings for nubs
                translate([0, 0, -shell]) triangle_nubs(back=true, clearance=clearance);
                translate([0, 0, height-frame_height]) triangle_nubs(clearance=clearance);
            }
            // inner section
            difference() {
                // floor
                rounded_triangle(fitting_top_d, base_length+shell, shell);
                // cutout
                difference() {
                    translate([0, 0, -1]) rounded_triangle(fitting_top_d-shell*2, base_length-shell*3, shell*3);
                    translate([0, base_length-fitting_top_d-4*shell, 0]) cube([base_length, base_length, shell*5], center=true);
                    translate([0, -shell*4, 0]) cube([shell*3, base_length, shell*4], center=true);
                }
            }
            // inlet fitting
            translate([0, coupling_y, 0]) {
                cylinder(d=fitting_top_d+shell+post_d*2, h=height-post_h);
            }
            // bumps for holding filter off ring
            d=fitting_top_d+post_d*1.5;
            translate([0, coupling_y, height-post_h]) for (angle=[120:60:240]) {
                translate([sin(angle)*d/2, cos(angle)*d/2, 0]) post(radius=post_d/2, height=post_h);
            }
        }
        translate([0, coupling_y, 0]) {
            // air inlet
            translate([0, 0, -post_h]) cylinder(d1=fitting_base_d, d2=fitting_top_d, h=height+0.02);
            // chamfer top of fitting hole inlet for airflow
            translate([0, 0, height - (post_h + post_d/2) + 0.01]) cylinder(d1=fitting_base_d, d2=fitting_base_d+post_d/2, h=post_d/2);
            // chamfer mask fitting hole inlet for fit
            translate([0, 0, height/16-fitting_base_d]) cylinder(d1=fitting_base_d*2, d2=fitting_base_d, h=fitting_base_d);
        }
        // back seal groove
        translate([0, fitting_top_d, 0]) cube([base_length, shell*2, shell], center=true);
    }
}
module triangle_frame(back=false) {
    // frame body
    difference() {
        rounded_triangle(fitting_top_d+interference+clearance, b_outer, frame_height);
        translate([0, 0, shell]) rounded_triangle(fitting_top_d, b_basis, frame_height);
        difference() {
            translate([0, 0, -1]) rounded_triangle(fitting_top_d, b_inner, frame_height*2);
            if (back) {
                translate([0, base_length-fitting_top_d-4*shell, 0]) cube([base_length, base_length, shell*5], center=true);
                translate([0, -shell*4, 0]) cube([shell*3, base_length/2+frame_extra_outer, shell*4], center=true);
            }
        }
        if (back) {
            translate([0, coupling_y, -0.1]) cylinder(d=fitting_top_d+shell*2, h=height-post_h);
        }
    }
    // back seal
    if (back) {
        intersection() {
            translate([0, fitting_top_d, shell]) cube([base_length, shell, shell/2], center=true);
            rounded_triangle(fitting_top_d+interference+clearance, b_outer, frame_height);
        }
    }
    triangle_nubs(back);
}

module triangle_set() {
    triangle_body();
    // demonstrate assembly
    //translate([0, 0, -shell]) triangle_frame(back=true);
    //translate([0, 0, height]) rotate([0, 180, 0]) triangle_frame();
    translate([base_length/2+2.7*shell, base_length*.7, 0]) rotate([0, 0, 180]) triangle_frame(back=true);
    translate([-(base_length/2+2.7*shell), base_length*.7, 0]) rotate([0, 0, 180]) triangle_frame();
}

rotate([0, 0, 60]) triangle_set();
