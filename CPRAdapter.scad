// Copyright Michael K Johnson
// License: Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// https://creativecommons.org/licenses/by-nc/4.0/

// LEGAL DISCLAIMER: The 3D printed mask information presented here is 
// intended to assist the general public during the current global
// pandemic related to COVID-19 and the related nationwide shortage
// of personal protective equipment.  Please be aware that this mask
// design is not intended to replace standard protective equipment such
// as N-95 masks or surgical masks when that equipment is available.
// The use of these 3D printed masks has not been fully tested and
// Michael K Johnson and any other contributors assume no liability and
// make no representations, warranties, or guarantees regarding the
// safety, efficacy, or appropriate use of these masks in any particular
// situation. Each facility should test each batch of masks prior to
// relying on them for protection. Use of this information for any
// purpose is at the maker’s and user’s own risk.

include <Core.scad>

experimental_fit_factor=1.4; // may have to tune for specific printer to get tight fit into mask
standoff = shell*3; // At least one shell for back frame; more for variation and/or strap attachment

// Some CPR masks have a female 22mm fitting; this is a male-male 22mm fitting
module cpr_adapter() {
    small_d = fitting_small_d + clearance*experimental_fit_factor;
    large_d = fitting_large_d + clearance*experimental_fit_factor;
    difference() {
        union() {
            cylinder(d1=small_d, d2=large_d, h=fitting_length);
            translate([0, 0, fitting_length]) cylinder(d=large_d, h=standoff);
            translate([0, 0, fitting_length+standoff]) cylinder(d1=large_d, d2=small_d, h=fitting_length);
        }
        translate([0, 0, -0.01]) cylinder(d=fitting_id, h=fitting_length*2+standoff+0.02);
    }
}

cpr_adapter();