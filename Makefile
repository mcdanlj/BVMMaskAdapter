%.stl: %.scad
	openscad $< -o $@

all: CPRAdapter.stl SimplusAdapter.stl StrapHolder.stl Triangle.stl

CPRAdapter.stl: Core.scad
SimplusAdapter.stl: Core.scad
StrapHolder.stl: Core.scad
Triangle.stl: Core.scad
