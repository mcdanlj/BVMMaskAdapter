// Copyright Michael K Johnson
// License: Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// https://creativecommons.org/licenses/by-nc/4.0/

// LEGAL DISCLAIMER: The 3D printed mask information presented here is 
// intended to assist the general public during the current global
// pandemic related to COVID-19 and the related nationwide shortage
// of personal protective equipment.  Please be aware that this mask
// design is not intended to replace standard protective equipment such
// as N-95 masks or surgical masks when that equipment is available.
// The use of these 3D printed masks has not been fully tested and
// Michael K Johnson and any other contributors assume no liability and
// make no representations, warranties, or guarantees regarding the
// safety, efficacy, or appropriate use of these masks in any particular
// situation. Each facility should test each batch of masks prior to
// relying on them for protection. Use of this information for any
// purpose is at the maker’s and user’s own risk.

// If your printer does not print square features (for example, if the bed
// wobbles in X/Y while printing), the round fitting inlet will not be printed
// round and fit will fail. This is the critical dimension for this design.

// These two parameters may need to be adjusted for a particular printer configuration,
// if you are not using the recommended 0.4mm nozzle with 0.3mm layers, or if your
// printer is not well tuned
// * Thicker (taller) layers and wider extrusion have more ambiguity about the definition
//   of X/Y dimensions. `clearance` will need to be adjusted if the 22mm fitting does
//   not fit, or fits too loosely. 0.2mm is typical for a 0.4mm nozzle and 0.3mm layers.
// * The shell thickness needs to allow for at least 3 perimeters, and needs to create a
//   layer 2mm wide. Aliasing may result in a shell thinner than specified. Measure the
//   actual printed shell and adjust the `shell` parameter until the shell is at least
//   2mm thick.
clearance=0.2; // leave space for fit to account for imprecise extrusion width
shell=2; // general minimum thickness of printed features
fitting_length=12;

reference_fitting_length=20; // length of fitting for reference taper
reference_fitting_small_d=22.2;
fitting_large_d=22.6;
// preserve measured taper over actual length
fitting_small_d=reference_fitting_small_d + (fitting_large_d-reference_fitting_small_d) * fitting_length/reference_fitting_length;

fitting_id=17;
fitting_base_d=fitting_large_d + 2*clearance;
fitting_top_d=fitting_small_d + 2*clearance;

// height giving cylindrical surface area equivalent to area of inner diameter for airflow
airflow_height = pow(fitting_id/2, 2) * 3.14 / (fitting_id*3.14);
post_d=airflow_height; // diameter of airflow posts
post_h=post_d + 3;

height=fitting_length + post_h;
frame_height=10;
interference=1; // extra radius between frame and adapter to fit cloth
spacing=post_d * 2.5; // spacing between airflow posts

$fn=60;
fns=12; // $fn for small features

module post(radius, height) {
    cylinder(r=radius, h=height-radius, $fn=fns);
    translate([0, 0, height-radius]) sphere(r=radius, $fn=fns);
    //cylinder(r=radius, h=height, $fn=fns);
}
