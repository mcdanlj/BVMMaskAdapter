This is an attempt to make an adapter to use filter fabric,
particularly Halyard H600 surgical wrap, and a BVM or CPAP mask,
as an emergency repirator replacement.  It has, as of the time of
writing, not been evaluated by any professional.  It is intended
for emergency use only, when no certified alternatives are available.

This model is intended to be printed as modeled, with typical
default settings, minimal infill, and no supports.

LEGAL DISCLAIMER: The 3D printed mask information presented here is
intended to assist the general public during the current global
pandemic related to COVID-19 and the related nationwide shortage
of personal protective equipment.  Please be aware that this mask
design is not intended to replace standard protective equipment such
as N-95 masks or surgical masks when that equipment is available.
The use of these 3D printed masks has not been fully tested and
Michael K Johnson and any other contributors assume no liability and
make no representations, warranties, or guarantees regarding the
safety, efficacy, or appropriate use of these masks in any particular
situation. Each facility should test each batch of masks prior to
relying on them for protection. Use of this information for any
purpose is at the maker’s and user’s own risk.

https://3dprint.nih.gov/discover/3dpx-014436

Copyright Michael K Johnson
License: [Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)](https://creativecommons.org/licenses/by-nc/4.0/)
