// Copyright Michael K Johnson
// License: Attribution-NonCommercial 4.0 International (CC BY-NC 4.0)
// https://creativecommons.org/licenses/by-nc/4.0/

// LEGAL DISCLAIMER: The 3D printed mask information presented here is 
// intended to assist the general public during the current global
// pandemic related to COVID-19 and the related nationwide shortage
// of personal protective equipment.  Please be aware that this mask
// design is not intended to replace standard protective equipment such
// as N-95 masks or surgical masks when that equipment is available.
// The use of these 3D printed masks has not been fully tested and
// Michael K Johnson and any other contributors assume no liability and
// make no representations, warranties, or guarantees regarding the
// safety, efficacy, or appropriate use of these masks in any particular
// situation. Each facility should test each batch of masks prior to
// relying on them for protection. Use of this information for any
// purpose is at the maker’s and user’s own risk.

include <Core.scad>

cpap_h=14; // longer than mask inlet length to account for printer variability
cpap_small_d=32; // diameter 10mm below outside of mask
cpap_end_d=33; // diameter at outside of mask
cpap_large_d=cpap_small_d + (cpap_end_d-cpap_small_d)*(cpap_h/10); // projected diameter at cpap_h to maintain taper
experimental_fit_factor=1.4; // may have to tune for specific printer to get tight fit into mask
standoff = shell*3; // At least one shell for back frame; more for variation and/or strap attachment

module simplus_adapter() {
    difference() {
        small_d = cpap_small_d + 2*clearance;
        large_d = cpap_large_d + 2*clearance;
        cylinder(d1=small_d, d2=large_d, h=cpap_h);
        translate([0, 0, -0.01]) cylinder(d1=small_d-2*shell, d2=fitting_id, h=cpap_h+0.02);
    }
    translate([0, 0, cpap_h]) {
        small_d = fitting_small_d + clearance*experimental_fit_factor;
        large_d = fitting_large_d + clearance*experimental_fit_factor;
        difference() {
            union() {
                cylinder(d=large_d, h=standoff);
                translate([0, 0, standoff]) cylinder(d1=large_d, d2=small_d, h=fitting_length);
            }
            translate([0, 0, -0.01]) cylinder(d1=fitting_id, d2=large_d-2*shell, h=fitting_length+standoff+0.02);
        }
    }
}

simplus_adapter();